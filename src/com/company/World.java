package com.company;

import javafx.scene.canvas.GraphicsContext;

public class World {
    public int width;
    public int height;
    public int canvasWidth;
    public int canvasHeight;
    public int marginX;
    public int marginY;


    public World(int width, int height, int canvasWidth, int canvasHeight) {
        this.width=width;
        this.height=height;
        this.canvasWidth = canvasWidth;
        this.canvasHeight = canvasHeight;
    }

    //Rysuje prostokąt reprezentujący ziemię
    public void draw(GraphicsContext gc) {
        marginX = (canvasWidth-width)/2;
        marginY = (canvasHeight-height)/2;

        double posX[] = new double[4];
        double posY[] = new double[4];

        posX[0] = marginX;
        posX[1] = marginX;
        posX[2] = marginX+this.width;
        posX[3] = marginX+this.width;
        posY[0] = marginY;
        posY[1] = marginY+this.height;
        posY[2] = marginY+this.height;
        posY[3] = marginY;

        gc.strokePolygon(posX,posY,4);
    }
}