package com.company;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

import java.util.*;

public class Antenna extends Object {
    public final int A = 100;
    public final int n = 10;
    public int id;

    public Antenna(int x, int y, int id) {
        this.x = x;
        this.y = y;
        this.id = id;
    }

    //Rysuje wielokąt z antenami jako wierzchołkami
    public static void drawPolygon(GraphicsContext gc, World world, List<Antenna> antennas) {
        final Color color = Color.BLUE;
        final Double opacity = 0.1;
        final int amountOfAntennas = antennas.size();

        double posX[] = new double[amountOfAntennas];
        double posY[] = new double[amountOfAntennas];

        int i = 0;
        ListIterator<Antenna> iterator = antennas.listIterator();

        while (iterator.hasNext()) {
            Antenna antenna = iterator.next();
            posX[i] = world.marginX + antenna.x;
            posY[i] = world.marginY + antenna.y;
            i++;
        }

        gc.setFill(color);
        gc.setGlobalAlpha(opacity);

        gc.fillPolygon(posX,posY,amountOfAntennas);

        gc.setFill(color.BLACK);
        gc.setGlobalAlpha(1.0);
    }

    //Rysuje wszystkie punkty reprezentujące anteny
    public static void drawAntennas(GraphicsContext gc, World world, List<Antenna> antennas) {
        int radius = 5;
        final Color color = Color.BLUE;

        gc.setFill(color);
        ListIterator<Antenna> iterator = antennas.listIterator();
        while (iterator.hasNext()) {
            Antenna antenna = iterator.next();
            gc.fillOval(world.marginX+antenna.x-radius, world.marginY+antenna.y-radius, 2*radius,2*radius);
        }

        gc.setFill(color.BLACK);
    }
}