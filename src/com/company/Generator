package com.company;

import java.util.*;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class Generator {
    private Random generator;
    List<Antenna> antennas;
    public Robot robot;
    public final int amountOfAntennas = 3;
    public World world;
    int randWidth;
    int randHeight;

    public Generator(int canvasWidth, int canvasHeight) {
        generator = new Random();
        antennas = new LinkedList<>();
        int randX;
        int randY;

        //Tworzenie ziemii
        randWidth = generator.nextInt((int)(0.6*canvasWidth)) + (int)(0.4*canvasWidth);
        randHeight = generator.nextInt((int)(0.4*canvasHeight)) + (int)(0.6*canvasHeight);
        world = new World(randWidth, randHeight, canvasWidth, canvasHeight);
        System.out.println("Wymiary ziemii: " + world.width + " x " + world.height + "\n");

        //Tworzenie anten
        int pos[][] = triangleGenerator(randWidth,randHeight, 30 ,0.4,0.7);
        for (int i = 0; i < amountOfAntennas; i++) {
            antennas.add(new Antenna(pos[i][0], pos[i][1], i+1));
            System.out.println("Położenie anteny " + (i + 1) + ") (" + antennas.get(i).x + ", " + antennas.get(i).y + ")");
        }

        //Tworzenie robota
        randX = generator.nextInt(randWidth-1)+1;
        randY = generator.nextInt(randHeight-1)+1;
        robot = new Robot(randX, randY, antennas);
    }

    //Generuje trójkąty o minimalnym kącie minAngle i o odługości boków z zadanego przedziału (minLength i maxLength przyjmują wartości [0;1])
    public int[][] triangleGenerator(int maxWidth, int maxHeight, double minAngle, double minLength, double maxLength) {
        int pos[][] = new int [3][2];
        generator = new Random();

        while (true) {
            for (int i=0; i<3; i++) {
                pos[i][0] = generator.nextInt(randWidth);
                pos[i][1] = generator.nextInt(randHeight);
            }
            if (checkMinDistances(pos, minLength, randWidth, randHeight)) {
                if (checkMaxDistances(pos, maxLength, randWidth, randHeight)) {
                    if (checkAngles(30, pos)) break;
                }
            }
        }
        return pos;
    }

    //Sprawdza czy kąty między bokami trójkąta nie są mniejsze od kąta przekazanego w parametrze minAngle
    private Boolean checkAngles(double minAngle, int[][] points) {
        double angle;
        for (int i=0; i<3; i++) {
            angle = Math.toDegrees(Math.acos(
                    (Math.pow(distance(points[(0+i)%3],points[(1+i)%3]),2)
                            +Math.pow(distance(points[(0+i)%3],points[(2+i)%3]),2)
                            -Math.pow(distance(points[(1+i)%3],points[(2+i)%3]),2))/
                            (2*distance(points[(0+i)%3],points[(1+i)%3])*distance(points[(0+i)%3],points[(2+i)%3]))));
            if (angle<minAngle) return false;
        }
        return true;
    }

    //Sprawdza czy boki trójkąta nie są mniejsze od zadanego ułamka najkrótszego boku ziemii
    private Boolean checkMinDistances(int [][] points, double fraction, int width, int height) {
        double minLen;
        if (width<height) {
            minLen = fraction*width;
        }
        else minLen = fraction*height;
        if (distance(points[0], points[1])<minLen) return false;
        if (distance(points[1], points[2])<minLen) return false;
        if (distance(points[0], points[2])<minLen) return false;
        return true;
    }

    //Sprawdza czy boki trójkąta nie są większe od zadanego ułamka najkrótszego boku ziemii
    private Boolean checkMaxDistances(int [][] points, double fraction, int width, int height) {
        double maxLen;
        if (width>height) {
            maxLen = fraction*width;
        }
        else maxLen = fraction*height;
        if (distance(points[0], points[1])>maxLen) return false;
        if (distance(points[1], points[2])>maxLen) return false;
        if (distance(points[0], points[2])>maxLen) return false;
        return true;
    }

    //Zwraca odległość między punktami a b
    private double distance(int[] a, int[] b) {
        return Math.hypot(a[0]-b[0],a[1]-b[1]);
    }

}