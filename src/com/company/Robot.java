package com.company;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import java.util.List;
import java.util.ListIterator;

public class Robot extends Object {

    private List<Antenna> antennas;
    private double[] signalMemory;
    private double[] currentSignal;
    private double fuel = 50;
    private double[] vector = {1,1};
    private boolean isIn;

    public Robot(int x, int y, List<Antenna> antennas) {
        this.x=x;
        this.y=y;
        this.antennas = antennas;
        this.signalMemory = new double[this.antennas.size()];
        inTriangle();
    }

    public boolean getIsIn()
    {
        return isIn;
    }

    public double[] signalStrength() { //Stowrzyłem tablicę z sygnałami, dla poprawy wyglądu kodu - siła sygnału dla aktualnego położenia
        double signals[] = new double[this.antennas.size()];
        ListIterator<Antenna> it = this.antennas.listIterator();
        int i=0;
        while(it.hasNext())
        {
            Antenna singleAntenna = it.next();
            double distance = Math.hypot(this.x - singleAntenna.x, this.y - singleAntenna.y); //hypot zastępuje sqrt(pow()+pow())
            signals[i] = singleAntenna.A-singleAntenna.n*Math.log(distance);
            //System.out.println("Antena nr"+i+" "+signals[i]+" "+distance);
            i++;

        }
        return signals;
    }

    public void move(World world) //ruszanie się - gdzie, i jak, zależy od funkcji vector()
    {
        if(signalMemory[0] == 0)//jeśli to 1szy ruch, to ruszamy się o odgórnie ustalony wektor
        {
            signalMemory = signalStrength();//
            this.x += vector[0];
            this.y += vector[1];
            inTriangle();
            //currentSignal = signalStrength();
        }
        else
        {
            //
            double[] newVector = vector(); //pobieramy dane z nowego położenia, w funkcji vector() zapisuje się aktualne położenie do signalMemory, po tym, robot rusza sie o wyznaczony wektor
            this.x += newVector[0];
            this.y += newVector[1];
            //signalMemory = signalStrength();

        }

        //System.out.println("Zmiana x: " +this.x+" Zmiana y: "+this.y );
        //System.out.println("Sygnły: "+signalMemory[0]+" "+signalMemory[1]+" "+signalMemory[2]);
        try
        {
            Thread.sleep(4); //żeby nie wypluwało wszystkiego naraz
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        //this.draw(gc, world);
    }
    private double[] vector()
    {
        double[] signals = signalStrength();
        int where=0;
        for(int i=0;i<signals.length;i++)
        {
            //System.out.println("Aktualny sygnał: "+signals[i]+" Sygnał w pamieci: "+signalMemory[i]);
            if(signals[i] > signalMemory[i]) where++; //sprawdzamy, do ilu anten się zbliżamy
        }
        if(where == 0) //zmiana kierunku na przeciwny
        {
            this.vector[0] *= -1;
            this.vector[1] *= -1;
        }
        else if(where == 1) //zmiana tylko jednej sp. wektora - jeśli okaże sie, że źle wybrało, to w następnej iteracji się skoryguje(bo będzie np. oddalać się od 3 anten)
        {
            this.vector[0] *= -1;
            inTriangle();
        }
        else if(where == 2) //jest dobrze w razie czego nastąpi korekta, jak opisano wyżej
        {
            inTriangle();
        }
        else if(where == 3){} //??
        signalMemory = signals; //zapis sygnałów przed wykonaniem ruchu
        return vector;
    }
    public boolean inTriangle()
    {

        int proof;
        int globalproof=0;
        double step = 10; //krok - dałem duży ale można zmieniś na mały
        int directions = 0;
        for(int i=-1;i<=1;i+=2) //pętla która umożliwia przemieszczenie się w wszystkich 4 kierunkach
        {
            for(int j=-1;j<=1;j+=2)
            {
                proof=0;
                this.x += i*step;
                this.y += j*step;
                double[] signals = signalStrength();
                //System.out.println("Iteratoty: i: "+i+" j: "+j);
                //System.out.println("x: "+this.x+" y: "+this.y);
                //System.out.println(" sygnały:"+signals[0]+" "+signals[1]+" "+signals[2]);
                //System.out.println("SigMem:"+signalMemory[0]+" "+signalMemory[1]+" "+signalMemory[2]);

                for(int k=0;k<signals.length;k++)
                    if(signals[k] < signalMemory[k]) {proof++;globalproof++;} //proof określa, od ilu anten się oddala w każdym kieunku z osobna
                //globalproof sumuje, ile oddaleń zanotowano łącznie - trudno określić, czy to 6 czy 7, dla różnych trójkątów różnie
                //System.out.println("Proof: "+proof);

                if(proof == 3 || signalMemory[0] == 0) //dla 1go razu kiedy signalMemory = 0.0 jest to zabezpieczenie
                    directions++; //jeśli oddala się od 3 anten, dodaje się directions, która jest przekazana do kolejnego warunku

                this.x -= i*step;//wracamy do punktu początkowego
                this.y -= j*step;
            }

        }

        if(directions > 0 || globalproof <= 7) this.isIn =false; // isIN = false, czyli robot nie jest w trójkącie
        else this.isIn =true;
        //System.out.println("Oddala się w kierunkach: "+ directions+" isIn: "+isIn);
        return isIn;
    }

    //Rysowanie robota
    public void draw(GraphicsContext gc, World world) {
        int radius = 4;
        final Color color = Color.BLACK;
        final double opacity = 0.5;

        gc.setGlobalAlpha(opacity);
        gc.setFill(color);
        gc.fillOval(world.marginX+this.x-radius, world.marginY+this.y-radius, 2*radius,2*radius);

        gc.setGlobalAlpha(1.0);
        gc.setFill(color.BLACK);
    }

    //Rysowanie robota (z parametrem color)
    public void draw(GraphicsContext gc, World world, Color color) {
        int radius = 4;
        gc.setGlobalAlpha(1.0);
        gc.setFill(color);
        gc.fillOval(world.marginX+this.x-radius, world.marginY+this.y-radius, 2*radius,2*radius);
        gc.setFill(color.BLACK);
    }

    //Przywrócenie robota do pozycji początkowej
    public void relocateRobot(int x, int y) {
        this.x = x;
        this.y = y;
        this.isIn = false;
    }
}