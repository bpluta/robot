package com.company;

import com.sun.tools.javah.Gen;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import javafx.animation.*;
import javafx.beans.property.*;
import javafx.util.Duration;

import java.security.Key;
import java.util.*;

public class Main extends Application {
    public static final int canvasWidth = 500;
    public static final int canvasHeight = 500;

    @Override public void start(Stage stage) {

        Timeline timeline = new Timeline();
        final Canvas canvas = new Canvas(canvasWidth, canvasHeight);

        Generator gen = new Generator(canvasWidth, canvasHeight);
        int robotInitX = gen.robot.x;
        int robotInitY = gen.robot.y;


        /* //Przełożenie położenia robota po każdym move() do listy KeyFrames i dodanie ich do Timeline (nie działa tak jak powinno)
        DoubleProperty x = new SimpleDoubleProperty();
        DoubleProperty y = new SimpleDoubleProperty();

        List<KeyFrame> keyFrames = new LinkedList<>();
        keyFrames.add(new KeyFrame(Duration.millis(0),new KeyValue(x, gen.world.marginX+gen.robot.x),new KeyValue(y, gen.world.marginY+gen.robot.y)));

        int i=1;
        while (!gen.robot.getIsIn()) {
            gen.robot.move(gen.world);
            keyFrames.add(new KeyFrame(Duration.millis(i*10),new KeyValue(x, gen.world.marginX+gen.robot.x),new KeyValue(y, gen.world.marginY+gen.robot.y)));
            i++;
        }

        gen.robot.relocateRobot(robotInitX, robotInitY);
        keyFrames.add(new KeyFrame(Duration.millis(i*10), new KeyValue(x, gen.world.marginX+gen.robot.x), new KeyValue(y, gen.world.marginY+gen.robot.y)));

        ListIterator<KeyFrame> it = keyFrames.listIterator();

        while (it.hasNext()) {
            KeyFrame keyFrame = it.next();
            timeline.getKeyFrames().add(keyFrame);
        }*/

        timeline.setCycleCount(Timeline.INDEFINITE);

        AnimationTimer timer = new AnimationTimer() {
            @Override
            public void handle(long now) {
                GraphicsContext gc = canvas.getGraphicsContext2D();

                //Rysowanie wszystkich elementów statycznych
                gc.setFill(Color.WHITE);
                gc.fillRect(0, 0, canvasWidth, canvasHeight);
                gen.world.draw(gc);
                Antenna.drawPolygon(gc, gen.world, gen.antennas);
                Antenna.drawAntennas(gc, gen.world, gen.antennas);

                //Rysowanie robota - po dotarciu do środka trójkąta czeka 0.3s, po czym ustawiany jest na pierwotnej pozycji
                gen.robot.draw(gc, gen.world);

                if (!gen.robot.getIsIn()) {
                    gen.robot.move(gen.world);
                }
                else {
                    try {
                        Thread.sleep(300);
                    }
                    catch (Exception e){
                        e.printStackTrace();
                    }
                    gen.robot.relocateRobot(robotInitX, robotInitY);
                }

                /* //Rysowanie robota przy użyciu KeyFrames
                gc.setFill(Color.BLACK);
                gc.setGlobalAlpha(0.5);

                gc.fillOval(x.doubleValue(), y.doubleValue(), 8, 8);
                */
            }
        };

        stage.setScene(new Scene(new Group(canvas)));
        stage.show();

        timer.start();
        timeline.play();
    }
    public static void main(String[] args) {
        launch(args);
    }
}
